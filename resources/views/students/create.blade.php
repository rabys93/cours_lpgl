
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ecole</title>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>
<body>
<div class="container border col-sm-4 mt-5">

    <h3 class="text-center text-info mt-5">Nouveau etudiant </h3>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form class="form-inline"  method="POST" action="{{route('store_student')}}">
        @csrf
        <div class="mb-3">
            <label for="first_name" class="form-label">Prénom</label>
            <input type="text" name="first_name" class="form-control" id="first_name" placeholder="Fatima">
        </div>
        <div class="mb-3">
            <label for="last_name" class="form-label">Nom</label>
            <input type="text" name="last_name" class="form-control" id="last_name" placeholder="Diallo">
        </div>
        <div class="mb-3">
            <label for="email" class="form-label">Email</label>
            <input type="email" name="email" class="form-control" id="email" placeholder="Diallo">
        </div>
        <div class="mb-3">
            <label for="phone" class="form-label">Téléphone</label>
            <input type="phone" name="phone" class="form-control" id="phone" placeholder="Diallo">
        </div>
        <div class="mb-3 text-center">
            <button type="submit" class="btn btn-lg btn-info">Enregistrer</button>
        </div>

    </form>
    <div class="card-footer">
        <a href="{{ route('index') }}" class=""><i class="fa fa-plus"></i> Retour </a>
    </div>

</div>
</body>
</html>
