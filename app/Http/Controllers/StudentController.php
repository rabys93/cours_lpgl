<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class StudentController extends Controller
{
    public function index()
    {
        $students = Student::all();

        return view('students.index', compact('students'));
    }

//    Cette fonction permet d'afficher la le formulaire etudiant

    public function create()
    {
        return view('students.create');
    }

    // Cette fonction permet d'enrégistrer les informations du formulaire sur la D

    public function store(Request $request)
    {

        try {
            $validatedData = $request->validate([
                'first_name' => ['required', 'min:3'],
                'last_name' => ['required', 'min:3'],
                'email' => ['required', 'email'],
                'phone' => ['required'],
            ]);

             Student::create($validatedData);

            return redirect()->route('index');

        } catch (ValidationException $e) {

           return redirect()->back()->withErrors($e->validator)->withInput();
        }

    }
}
